import java.util.Scanner;

public class XO {

	public static void main(String[] args) {
		char turn = 'O';
		showWelcome();
		char[][] table = new char[][] { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
		for (;;) {
			showTable(table);
			if (checkWin(table, turn)) {
				break;
			}
			turn = switchTurn(turn);
			System.out.println("It's " + turn + " turn.");
			inputRowCol(table, turn);
		}
		showBye();
	}

	public static void showWelcome() {
		System.out.println("!! Welcome to XO Game !!");
	}

	public static void showTable(char[][] table) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(table[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static boolean checkWin(char[][] table, char turn) {
		if (checkRow(table)) {
			System.out.println("Congratulations!! "+turn + " is Winner!!");
			return true;
		} else if (checkCol(table)) {
			System.out.println("Congratulations!! "+turn + " is Winner!!");
			return true;
		} else if (checkDiag(table)) {
			System.out.println("Congratulations!! "+turn + " is Winner!!");
			return true;
		} else {
			return false;
		}
	}

	public static boolean checkRow(char[][] table) {
		if (table[0][0] == table[0][1] && table[0][1] == table[0][2] && table[0][0] != '-') {
			return true;
		} else if (table[1][0] == table[1][1] && table[1][1] == table[1][2] && table[1][0] != '-') {
			return true;
		} else if (table[2][0] == table[2][1] && table[2][1] == table[2][2] && table[2][0] != '-') {
			return true;
		} else {
			return false;
		}
	}

	public static boolean checkCol(char[][] table) {
		if (table[0][0] == table[1][0] && table[1][0] == table[2][0] && table[0][0] != '-') {
			return true;
		} else if (table[0][1] == table[1][1] && table[1][1] == table[2][1] && table[0][1] != '-') {
			return true;
		} else if (table[0][2] == table[1][2] && table[1][2] == table[2][2] && table[0][2] != '-') {
			return true;
		} else {
			return false;
		}
	}

	public static boolean checkDiag(char[][] table) {
		if (table[0][0] == table[1][1] && table[1][1] == table[2][2] && table[0][0] != '-') {
			return true;
		} else if (table[2][0] == table[1][1] && table[1][1] == table[0][2] && table[2][0] != '-') {
			return true;
		} else {
			return false;
		}
	}

	public static char switchTurn(char turn) {
		if (turn == 'X') {
			turn = 'O';
		} else {
			turn = 'X';
		}
		return turn;
	}

	public static void inputRowCol(char[][] table, char turn) {
		Scanner kb = new Scanner(System.in);
		for (;;) {
			System.out.print("Please input row, column : ");
			int inputRow = kb.nextInt();
			int inputCol = kb.nextInt();
			if (checkBound(inputRow, inputCol)) {
				if (checkNull(inputRow, inputCol, table, turn)) {
					break;
				}
			}
		}
	}

	public static boolean checkBound(int r, int c) {
		for (;;) {
			if ((r >= 1 && r <= 3) && (c >= 1 && c <= 3)) {
				return true;
			} else {
				System.out.println("You can input only 1 2 or 3");
				return false;
			}
		}
	}

	public static boolean checkNull(int r, int c, char[][] table, char turn) {
		for (;;) {
			if (table[r - 1][c - 1] == '-') {
				table[r - 1][c - 1] = turn;
				return true;
			} else {
				System.out.println("Not null! input again");
				return false;
			}
		}
	}

	public static void showBye() {
		System.out.println("Thank you, Bye!!");
	}

}
